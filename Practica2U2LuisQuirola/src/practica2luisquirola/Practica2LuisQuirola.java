/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package practica2luisquirola;

import controlador.DAO.GeneroDao;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mt3k
 */
public class Practica2LuisQuirola {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            GeneroDao gd = new GeneroDao();
            gd.getGenero().setNombre("BOLERO");
            gd.guardar();
            gd.getGenero().setNombre("CUMBIA");
            gd.guardar();
            gd.getGenero().setNombre("JAZZ");
            gd.guardar();
            gd.getGenero().setNombre("MERENGUE");
            gd.guardar();
           
        } catch (IOException ex) {
            Logger.getLogger(Practica2LuisQuirola.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
