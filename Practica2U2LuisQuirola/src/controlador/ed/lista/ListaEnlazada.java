/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.ed.lista;

import controlador.ed.lista.exception.PosicionException;
import controlador.ed.lista.exception.VacioException;
import java.lang.reflect.Array;

/**
 *
 * @author alyce
 */
public class ListaEnlazada<E> {

    private NodoLista<E> cabecera;
    private Integer size = 0;

    public NodoLista getCabecera() {
        return cabecera;
    }

    public void setCabecera(NodoLista cabecera) {
        this.cabecera = cabecera;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public boolean estaVacia() {
        return cabecera == null;
    }

    public boolean insertar(E info) {
        NodoLista<E> nuevo = new NodoLista<>(null, info);
        if (estaVacia()) {

            this.cabecera = nuevo;
            this.size++;
        } else {
            NodoLista<E> aux = cabecera;
//            for (int i = 0; i < size()-1; i++) {
//                aux = aux.getSig();
//            }
            while (aux.getSig() != null) {
                aux = aux.getSig();
            }
            aux.setSig(nuevo);
            this.size++;
        }
        return true;
    }

    /**
     *
     * @return
     */
    public Integer size() {

        return size;
    }

    public  <E> int compararElementos(E elemento1, E elemento2) {

        if (elemento1.equals(elemento2)) {
            return 0;
        } else {
            return -1;
        }
    }
    public E deleteInicio() throws VacioException {
        if (estaVacia()) {
            throw new VacioException("La lista está vacía");
        }

        E elemento = cabecera.getInfo();
        cabecera = cabecera.getSig();
        size--;

        return elemento;
    }

    public void imprimir() throws VacioException {
        if (estaVacia()) {
            throw new VacioException();
        } else {
            NodoLista<E> aux = cabecera;
            System.out.println("----------Lista------------");
            for (int i = 0; i < size(); i++) {
                System.out.println(aux.getInfo());
                aux = aux.getSig();
            }
            System.out.println("----------Lista fin--------------");
        }
    }

    public void insertarInicio(E info) {
        if (estaVacia()) {
            insertar(info);
        } else {
            NodoLista<E> nuevo = new NodoLista<>(null, info);
            nuevo.setSig(cabecera);
            cabecera = nuevo;
            size++;

        }
    }

    public void insertarPosicion(E info, Integer pos) throws PosicionException {
        if (estaVacia()) {
            insertar(info);
        } else if (pos >= 0 && pos < size() && pos == 0) {
            insertarInicio(info);
        } else if (pos >= 0 && pos < size()) {
            NodoLista<E> nuevo = new NodoLista<>(null, info);
            NodoLista<E> aux = cabecera;
            for (int i = 0; i < (pos - 1); i++) {
                aux = aux.getSig();
            }
            NodoLista<E> sig = aux.getSig();
            aux.setSig(nuevo);
            nuevo.setSig(sig);
            size++;
        } else {
            throw new PosicionException();
        }
    }

    public E get(Integer pos) throws VacioException, PosicionException {
        if (estaVacia()) {
            throw new VacioException();
        } else {
            E dato = null;
            if (pos >= 0 && pos < size()) {
                if (pos == 0) {
                    dato = cabecera.getInfo();
                } else {
                    NodoLista<E> aux = cabecera;
                    for (int i = 0; i < pos; i++) {
                        aux = aux.getSig();
                    }
                    dato = aux.getInfo();
                }
            } else {
                throw new PosicionException();
            }
            return dato;
        }

    }

    public E delete(Integer pos) throws VacioException, PosicionException {
        if (estaVacia()) {
            throw new VacioException();
        } else {
            E dato = null;
            if (pos >= 0 && pos < size()) {
                if (pos == 0) {
                    dato = cabecera.getInfo();
                    cabecera = cabecera.getSig();
                    size--;
                } else {
                    NodoLista<E> aux = cabecera;
                    for (int i = 0; i < (pos - 1); i++) {
                        aux = aux.getSig();
                    }
                    if ((pos + 1) == size()) {
                        NodoLista<E> aux1 = aux.getSig();
                        // dato = aux.getSig().getInfo();
                    } else {
                        dato = aux.getInfo();
                    }
                    dato = aux.getInfo();
                    NodoLista<E> proximo = aux.getSig();
                    aux.setSig(proximo.getSig());
                    size--;
                }
            } else {

                throw new PosicionException();
            }
            return dato;
        }

    }

    public void deleteAll() {
        this.cabecera = null;

        this.size = 0;
    }

    public void vaciar() {
        cabecera = null;
        size = 0;
    }

    public E[] toArray() {
        Class<E> clazz = null;
        E[] matriz = null;
        if (this.size > 0) {
            matriz = (E[]) java.lang.reflect.Array.newInstance(cabecera.getInfo().getClass(), this.size);
            NodoLista<E> aux = cabecera;
            for (int i = 0; i < this.size; i++) {
                matriz[i] = aux.getInfo();
                aux = aux.getSig();
            }
        }
        return matriz;
    }

    public ListaEnlazada<E> toList(E[] matriz) {
        this.deleteAll();
        for (int i = 0; i < matriz.length; i++) {
            this.insertar(matriz[i]);
        }
        return this;
    }

    public void update(Integer pos, E dato) throws VacioException, PosicionException {
        if (estaVacia()) {
            throw new VacioException();
        } else {
            if (pos >= 0 && pos < size()) {
                if (pos == 0) {
                    dato = cabecera.getInfo();

                } else {
                    NodoLista<E> aux = cabecera;
                    for (int i = 0; i < pos; i++) {
                        aux = aux.getSig();
                    }
                    aux.setInfo(dato);
                }
            } else {
                throw new PosicionException();
            }
        }

    }

    public E eliminarPrimero() throws VacioException {
        if (estaVacia()) {
            throw new VacioException("La lista está vacía");
        }

        NodoLista<E> nodoEliminado = cabecera;
        cabecera = cabecera.getSig();

        return nodoEliminado.getInfo();
    }

    public E getPrimero() throws VacioException {
        if (estaVacia()) {
            throw new VacioException("La lista está vacía.");
        }
        return (E) cabecera.getSig().getInfo();
    }

    public E get1(int indice) throws PosicionException {
        if (indice < 0 || indice >= size) {
            throw new PosicionException("Índice fuera de rango: " + indice);
        }

        NodoLista<E> actual = cabecera;
        for (int i = 0; i < indice; i++) {
            actual = actual.getSig();
        }

        return actual.getInfo();
    }

  

    public void concatenarListas(ListaEnlazada<Object> lista1, ListaEnlazada<Object> lista2) {
        if (lista1.estaVacia()) {
            lista1.setCabecera(lista2.getCabecera());
        } else {
            NodoLista<Object> nodoActual = lista1.getCabecera();
            while (nodoActual.getSig() != null) {
                nodoActual = nodoActual.getSig();
            }
            nodoActual.setSig(lista2.getCabecera());
        }

    }

//    public void intercambioElementos(int indice1, int indice2) throws VacioException, PosicionException {
//        if (indice1 < 0 || indice1 >= size || indice2 < 0 || indice2 >= size) {
//            throw new PosicionException("Indices Invalidos");
//        }
//
//        if (indice1 == indice2) {
//            return; // No es necesario intercambiar elementos si los índices son iguales
//        }
//
//        NodoLista<E> nodo1 = (NodoLista<E>) get1(indice1);
//        NodoLista<E> nodo2 = (NodoLista<E>) get1(indice2);
//
//        E temp = nodo1.getInfo();
//        nodo1.setInfo(nodo2.getInfo());
//        nodo2.setInfo(temp);
//    }
    public void set(int posicion, E elemento) throws PosicionException, VacioException {
        if (estaVacia()) {
            throw new VacioException("La lista está vacía.");
        }

        if (posicion < 0 || posicion >= size) {
            throw new PosicionException("La posición especificada está fuera de rango.");
        }

        NodoLista<E> nodoActual = cabecera;
        for (int i = 0; i < posicion; i++) {
            nodoActual = nodoActual.getSig();
        }

        nodoActual.setInfo(elemento);
    }

    public void intercambioElementos(int posicion1, int posicion2) throws PosicionException, VacioException {
        E elemento1 = get(posicion1);
        E elemento2 = get(posicion2);

        set(posicion1, elemento2);
        set(posicion2, elemento1);
    }
}
