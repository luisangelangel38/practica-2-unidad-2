/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.DAO;

import controlador.ed.lista.ListaEnlazada;
import controlador.ed.lista.exception.PosicionException;
import controlador.ed.lista.exception.VacioException;
import controlador.ordenacion.Quicksort;
import java.io.File;
import java.io.IOException;
import modelo.Cantante;

/**
 *
 * @author mt3k
 */
public class CantanteDao<T> extends AdaptadorDAO<Cantante> {

    private Cantante cantante;

    public Cantante getCantante() {

        if (this.cantante == null) {
            this.cantante = new Cantante();
        }
        return cantante;
    }

    public void setCantante(Cantante cantante) {
        this.cantante = cantante;
    }

    public CantanteDao() {
        super(Cantante.class);
    }

    public void guardar() throws IOException {
        cantante.setId(generateID());
        this.guardar(cantante);
    }

    public void modificar(Integer pos) throws VacioException, PosicionException, IOException {
        this.modificar(cantante, pos);
    }

    private Integer generateID() {
        return listar().size() + 1;
    }

    //BUSQUEDA BINARIA
    public ListaEnlazada<Cantante> buscarPorNombreBinarioLista(ListaEnlazada<Cantante> lista, String nombre) throws VacioException, PosicionException {
        Cantante aux = busquedaBinariaNombre(lista, nombre);

        ListaEnlazada<Cantante> resultado = new ListaEnlazada<>();

        if (aux != null) {
            resultado.insertar(aux);
        }

        return resultado;
    }

    public ListaEnlazada<Cantante> ordenarNombre(ListaEnlazada<Cantante> lista) {
        try {
            Cantante[] matriz = lista.toArray();
            for (int i = 1; i < lista.size(); i++) {
                Cantante key = lista.get(i);
                int j = i - 1;
                while (j >= 0 && (matriz[j].getNombre().compareToIgnoreCase(key.getNombre())) > 0) {
                    //lista.update(j+1, lista.get(j));
                    matriz[j + 1] = matriz[j];
                    j = j - 1;
                }
                //lista.update(j+1, key);
                matriz[j + 1] = key;
            }
            lista.toList(matriz);
        } catch (Exception e) {
        }
        return lista;
    }

    public Cantante busquedaBinariaNombre(ListaEnlazada<Cantante> lista, String nombre) throws VacioException, PosicionException {
        Cantante cantante = null;
        int inicio = 0;
        int fin = lista.size() - 1;
        Quicksort quicksort = new Quicksort();
        lista = quicksort.quicksortNombre(lista, true);

        while (inicio <= fin) {
            int medio = inicio + (fin - inicio) / 2;

            cantante = lista.get(medio);
            int comparacion = cantante.getNombre().compareToIgnoreCase(nombre);

            if (comparacion == 0) {
                return cantante;
            } else if (comparacion < 0) {
                inicio = medio + 1;
            } else {
                fin = medio - 1;
            }
        }

        return null;
    }

    public ListaEnlazada<Cantante> buscarPorNacionalidadBinarioLista(ListaEnlazada<Cantante> lista, String nombre) throws VacioException, PosicionException {
        Cantante aux = busquedaBinariaNacionalidad(lista, nombre);

        ListaEnlazada<Cantante> resultado = new ListaEnlazada<>();

        if (aux != null) {
            resultado.insertar(aux);
        }

        return resultado;
    }

    public Cantante busquedaBinariaNacionalidad(ListaEnlazada<Cantante> lista, String nombre) throws VacioException, PosicionException {
        Cantante cantante = null;
        int inicio = 0;
        int fin = lista.size() - 1;
        Quicksort quicksort = new Quicksort();
        lista = quicksort.quicksortNacionalidad(lista, true);

        while (inicio <= fin) {
            int medio = inicio + (fin - inicio) / 2;

            cantante = lista.get(medio);
            int comparacion = cantante.getNacionalidad().compareToIgnoreCase(nombre);

            if (comparacion == 0) {
                return cantante;
            } else if (comparacion < 0) {
                inicio = medio + 1;
            } else {
                fin = medio - 1;
            }
        }

        return null;
    }

    public ListaEnlazada<Cantante> buscarPorGeneroBinarioLista(ListaEnlazada<Cantante> lista, String nombreGenero) throws VacioException, PosicionException {
        Cantante aux = busquedaBinariaGenero(lista, nombreGenero);

        ListaEnlazada<Cantante> resultado = new ListaEnlazada<>();

        if (aux != null) {
            resultado.insertar(aux);
        }

        return resultado;
    }

    public Cantante busquedaBinariaGenero(ListaEnlazada<Cantante> lista, String nombreGenero) throws VacioException, PosicionException {
        Cantante cantante = null;
        GeneroDao gd = new GeneroDao();
        int inicio = 0;
        int fin = lista.size() - 1;
        Quicksort quicksort = new Quicksort();
        lista = quicksort.quicksortGenero(lista, true);

        while (inicio <= fin) {
            int medio = inicio + (fin - inicio) / 2;

            cantante = lista.get(medio);
            Integer idGeneroCantante = cantante.getId_genero();
            String nombreGeneroCantante = gd.obtener(idGeneroCantante).getNombre();

            // Comparar el nombre del género
            int comparacion = nombreGeneroCantante.compareToIgnoreCase(nombreGenero);
            if (comparacion == 0) {
                return cantante;
            } else if (comparacion < 0) {
                inicio = medio + 1;
            } else {
                fin = medio - 1;
            }
        }

        return null;
    }

    public ListaEnlazada<Cantante> buscarPorEdadBinarioLista(ListaEnlazada<Cantante> lista, String edad) throws VacioException, PosicionException {
        Cantante aux = busquedaBinariaEdad(lista, edad);

        ListaEnlazada<Cantante> resultado = new ListaEnlazada<>();

        if (aux != null) {
            resultado.insertar(aux);
        }

        return resultado;
    }

    public Cantante busquedaBinariaEdad(ListaEnlazada<Cantante> lista, String edad) throws VacioException, PosicionException {
        Cantante cantante = null;
        int inicio = 0;
        int fin = lista.size() - 1;
        Quicksort quicksort = new Quicksort();
        lista = quicksort.quicksortEdad(lista, true);

        while (inicio <= fin) {
            int medio = inicio + (fin - inicio) / 2;

            cantante = lista.get(medio);
            int comparacion = Integer.compare(cantante.getEdad(), Integer.parseInt(edad));

            if (comparacion == 0) {
                return cantante;
            } else if (comparacion < 0) {
                inicio = medio + 1;
            } else {
                fin = medio - 1;
            }
        }

        return null;
    }

    public ListaEnlazada<Cantante> buscarPorNombreLinealLista(ListaEnlazada<Cantante> lista, String nombre) throws VacioException, PosicionException {
        ListaEnlazada<Cantante> resultado = new ListaEnlazada<>();
        busquedaLinealNombre(lista, nombre, resultado);
        return resultado;
    }

    public void busquedaLinealNombre(ListaEnlazada<Cantante> lista, String nombre, ListaEnlazada<Cantante> resultado) throws VacioException, PosicionException {
        int inicio = 0;
        int fin = lista.size() - 1;
        Quicksort quicksort = new Quicksort();
        lista = quicksort.quicksortNombre(lista, true);

        while (inicio <= fin) {
            int medio = inicio + (fin - inicio) / 2;

            Cantante cantante = lista.get(medio);
            int comparacion = cantante.getNombre().compareToIgnoreCase(nombre);

            if (comparacion == 0) {
                resultado.insertar(cantante);
                // Buscar más coincidencias hacia atrás
                int indice = medio - 1;
                while (indice >= 0 && lista.get(indice).getNombre().equalsIgnoreCase(nombre)) {
                    resultado.insertar(lista.get(indice));
                    indice--;
                }
                // Buscar más coincidencias hacia adelante
                indice = medio + 1;
                while (indice < lista.getSize() && lista.get(indice).getNombre().equalsIgnoreCase(nombre)) {
                    resultado.insertar(lista.get(indice));
                    indice++;
                }
                return;
            } else if (comparacion < 0) {
                inicio = medio + 1;
            } else {
                fin = medio - 1;
            }
        }
    }

    public ListaEnlazada<Cantante> buscarPorNacionalidadLinealLista(ListaEnlazada<Cantante> lista, String nombre) throws VacioException, PosicionException {
        ListaEnlazada<Cantante> resultado = new ListaEnlazada<>();
        busquedaLinealNacionalidad(lista, nombre, resultado);
        return resultado;
    }

    public void busquedaLinealNacionalidad(ListaEnlazada<Cantante> lista, String nombre, ListaEnlazada<Cantante> resultado) throws VacioException, PosicionException {
        int inicio = 0;
        int fin = lista.size() - 1;
        Quicksort quicksort = new Quicksort();
        lista = quicksort.quicksortNacionalidad(lista, true);

        while (inicio <= fin) {
            int medio = inicio + (fin - inicio) / 2;

            Cantante cantante = lista.get(medio);
            int comparacion = cantante.getNacionalidad().compareToIgnoreCase(nombre);

            if (comparacion == 0) {
                resultado.insertar(cantante);
                // Buscar más coincidencias hacia atrás
                int indice = medio - 1;
                while (indice >= 0 && lista.get(indice).getNacionalidad().equalsIgnoreCase(nombre)) {
                    resultado.insertar(lista.get(indice));
                    indice--;
                }
                // Buscar más coincidencias hacia adelante
                indice = medio + 1;
                while (indice < lista.getSize() && lista.get(indice).getNacionalidad().equalsIgnoreCase(nombre)) {
                    resultado.insertar(lista.get(indice));
                    indice++;
                }
                return;
            } else if (comparacion < 0) {
                inicio = medio + 1;
            } else {
                fin = medio - 1;
            }
        }
    }

    public ListaEnlazada<Cantante> buscarPorGeneroLinealLista(ListaEnlazada<Cantante> lista, String nombreGenero) throws VacioException, PosicionException {
        ListaEnlazada<Cantante> resultado = new ListaEnlazada<>();
        busquedaLinealGenero(lista, nombreGenero, resultado);
        return resultado;
    }

    public void busquedaLinealGenero(ListaEnlazada<Cantante> lista, String nombreGenero, ListaEnlazada<Cantante> resultado) throws VacioException, PosicionException {
        int inicio = 0;
        int fin = lista.size() - 1;
        Quicksort quicksort = new Quicksort();
        lista = quicksort.quicksortGenero(lista, true);

        while (inicio <= fin) {
            int medio = inicio + (fin - inicio) / 2;

            Cantante cantante = lista.get(medio);
            GeneroDao gd = new GeneroDao();
            Integer idGeneroCantante = cantante.getId_genero();
            String nombreGeneroCantante = gd.obtener(idGeneroCantante).getNombre();

            // Comparar el nombre del género
            int comparacion = nombreGeneroCantante.compareToIgnoreCase(nombreGenero);
            if (comparacion == 0) {
                resultado.insertar(cantante);
                // Buscar más coincidencias hacia atrás
                int indice = medio - 1;
                while (indice >= 0 && gd.obtener(lista.get(indice).getId_genero()).getNombre().equalsIgnoreCase(nombreGenero)) {
                    resultado.insertar(lista.get(indice));
                    indice--;
                }
                // Buscar más coincidencias hacia adelante
                indice = medio + 1;
                while (indice < lista.getSize() && gd.obtener(lista.get(indice).getId_genero()).getNombre().equalsIgnoreCase(nombreGenero)) {
                    resultado.insertar(lista.get(indice));
                    indice++;
                }
                return;
            } else if (comparacion < 0) {
                inicio = medio + 1;
            } else {
                fin = medio - 1;
            }
        }
    }

    public ListaEnlazada<Cantante> buscarPorEdadLinealLista(ListaEnlazada<Cantante> lista, String edad) throws VacioException, PosicionException {
        ListaEnlazada<Cantante> resultado = new ListaEnlazada<>();
        busquedaLinealEdad(lista, edad, resultado);
        return resultado;
    }

    public void busquedaLinealEdad(ListaEnlazada<Cantante> lista, String edad, ListaEnlazada<Cantante> resultado) throws VacioException, PosicionException {
        int inicio = 0;
        int fin = lista.size() - 1;
        Quicksort quicksort = new Quicksort();
        lista = quicksort.quicksortEdad(lista, true);

        while (inicio <= fin) {
            int medio = inicio + (fin - inicio) / 2;

            Cantante cantante = lista.get(medio);
            int comparacion = Integer.compare(cantante.getEdad(), Integer.parseInt(edad));

            if (comparacion == 0) {
                resultado.insertar(cantante);
                // Buscar más coincidencias hacia atrás
                int indice = medio - 1;
                while (indice >= 0 && lista.get(indice).getEdad() == Integer.parseInt(edad)) {
                    resultado.insertar(lista.get(indice));
                    indice--;
                }
                // Buscar más coincidencias hacia adelante
                indice = medio + 1;
                while (indice < lista.getSize() && lista.get(indice).getEdad() == Integer.parseInt(edad)) {
                    resultado.insertar(lista.get(indice));
                    indice++;
                }
                return;
            } else if (comparacion < 0) {
                inicio = medio + 1;
            } else {
                fin = medio - 1;
            }
        }
    }


}
