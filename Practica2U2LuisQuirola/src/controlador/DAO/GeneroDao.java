/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.DAO;

import controlador.ed.lista.ListaEnlazada;
import controlador.ed.lista.exception.PosicionException;
import controlador.ed.lista.exception.VacioException;
import java.io.IOException;
import modelo.Genero;

/**
 *
 * @author mt3k
 */
public class GeneroDao extends AdaptadorDAO<Genero> {

    private Genero genero;

    public GeneroDao() {
        super(Genero.class);
    }

    public void guardar() throws IOException {
        genero.setId(generateID());
        this.guardar(genero);
    }

    public void modificar(Integer pos) throws VacioException, PosicionException, IOException {
        this.modificar(genero, pos);
    }

    private Integer generateID() {
        return listar().size() + 1;
    }

    public Genero getGenero() {
        if (this.genero == null) {
            this.genero = new Genero();
        }
        return genero;
    }

    public void setGenero(Genero genero) {
        this.genero = genero;
    }

    public ListaEnlazada<Genero> ordenarNombre(ListaEnlazada<Genero> lista) {
        try {
            Genero[] matriz = lista.toArray();
            for (int i = 1; i < lista.size(); i++) {
                Genero key = lista.get(i);
                int j = i - 1;
                while (j >= 0 && (matriz[j].getNombre().compareToIgnoreCase(key.getNombre())) > 0) {
                    matriz[j + 1] = matriz[j];
                    j = j - 1;
                }
                matriz[j + 1] = key;
            }
            lista.toList(matriz);
        } catch (Exception e) {
            System.out.println("ERROR EN ORDENAR NOMBRE GD" + e);
        }
        return lista;

    }

    public Genero buscarPorNombre(String dato) throws Exception {
        Genero resultado = null;
        ListaEnlazada<Genero> lista = listar();
        for (int i = 0; i < lista.size(); i++) {
            Genero aux = lista.get(i);
            if (aux.getNombre().toLowerCase().equals(dato.toLowerCase())) {
                resultado = aux;
                break;
            }
        }
        return resultado;

    }

}
