/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.ordenacion;

import controlador.ed.lista.ListaEnlazada;
import controlador.ed.lista.exception.PosicionException;
import controlador.ed.lista.exception.VacioException;

/**
 *
 * @author mt3k
 */
public class Merge<E> {

    public ListaEnlazada<E> mergeSort(ListaEnlazada<E> lista, boolean ordenacion) throws VacioException, PosicionException {
        if (lista.getSize() <= 1) {
            return lista;
        }

        int medio = lista.getSize() / 2;
        ListaEnlazada<E> izquierda = new ListaEnlazada<>();
        ListaEnlazada<E> derecha = new ListaEnlazada<>();

        for (int i = 0; i < medio; i++) {
            izquierda.insertar(lista.get(i));
        }

        for (int i = medio; i < lista.getSize(); i++) {
            derecha.insertar(lista.get(i));
        }

        izquierda = mergeSort(izquierda, ordenacion);
        derecha = mergeSort(derecha, ordenacion);

        return mezclarListas(izquierda, derecha, ordenacion);
    }

    private ListaEnlazada<E> mezclarListas(ListaEnlazada<E> izquierda, ListaEnlazada<E> derecha, boolean ascendente) throws VacioException, PosicionException {
        ListaEnlazada<E> resultado = new ListaEnlazada<>();

        while (!izquierda.estaVacia() && !derecha.estaVacia()) {
            E elementoIzquierda = (E) izquierda.getCabecera().getInfo();
            E elementoDerecha = (E) derecha.getCabecera().getInfo();

            if (compararElementos(elementoIzquierda, elementoDerecha, ascendente) <= 0) {
                resultado.insertar(izquierda.eliminarPrimero());
            } else {
                resultado.insertar(derecha.eliminarPrimero());
            }
        }

        if (!izquierda.estaVacia()) {
            while (!izquierda.estaVacia()) {
                resultado.insertar(izquierda.eliminarPrimero());
            }
        }

        if (!derecha.estaVacia()) {
            while (!derecha.estaVacia()) {
                resultado.insertar(derecha.eliminarPrimero());
            }
        }

        return resultado;
    }

    private int compararElementos(E elemento1, E elemento2, boolean ascendente) {
        if (elemento1 instanceof String && elemento2 instanceof String) {
            String str1 = (String) elemento1;
            String str2 = (String) elemento2;
            return str1.compareTo(str2);
        } else if (elemento1 instanceof Integer && elemento2 instanceof Integer) {
            Integer int1 = (Integer) elemento1;
            Integer int2 = (Integer) elemento2;
            return int1.compareTo(int2);
        }
        return 0;
    }
}
