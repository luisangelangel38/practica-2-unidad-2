/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.ordenacion;

import controlador.DAO.GeneroDao;
import controlador.ed.lista.ListaEnlazada;
import controlador.ed.lista.NodoLista;
import controlador.ed.lista.exception.PosicionException;
import controlador.ed.lista.exception.VacioException;
import modelo.Cantante;

/**
 *
 * @author mt3k
 */
public class Quicksort<E> {

    public ListaEnlazada<Cantante> quicksortNombre(ListaEnlazada<Cantante> lista, boolean ascendente) throws VacioException, PosicionException {
        quicksortRecursivoNombre(lista, 0, lista.getSize() - 1, ascendente);
        return lista;
    }

    private void quicksortRecursivoNombre(ListaEnlazada<Cantante> lista, int inicio, int fin, boolean ascendente) throws VacioException, PosicionException {
        if (inicio < fin) {
            int indicePivote = particionNombre(lista, inicio, fin, ascendente);
            quicksortRecursivoNombre(lista, inicio, indicePivote - 1, ascendente);
            quicksortRecursivoNombre(lista, indicePivote + 1, fin, ascendente);
        }
    }

    private int particionNombre(ListaEnlazada<Cantante> lista, int inicio, int fin, boolean ascendente) throws VacioException, PosicionException {
        Cantante pivote = lista.get(fin);
        int i = inicio - 1;

        for (int j = inicio; j < fin; j++) {
            Cantante elementoActual = lista.get(j);
            if ((ascendente && elementoActual.getNombre().compareToIgnoreCase(pivote.getNombre()) <= 0)
                    || (!ascendente && elementoActual.getNombre().compareToIgnoreCase(pivote.getNombre()) >= 0)) {
                i++;
                lista.intercambioElementos(i, j);
            }
        }

        lista.intercambioElementos(i + 1, fin);
        return i + 1;
    }

    public ListaEnlazada<Cantante> quicksortNacionalidad(ListaEnlazada<Cantante> lista, boolean ascendente) throws VacioException, PosicionException {
        quicksortRecursivoNacionalidad(lista, 0, lista.getSize() - 1, ascendente);
        return lista;
    }

    private void quicksortRecursivoNacionalidad(ListaEnlazada<Cantante> lista, int inicio, int fin, boolean ascendente) throws VacioException, PosicionException {
        if (inicio < fin) {
            int indicePivote = particionNacionalidad(lista, inicio, fin, ascendente);
            quicksortRecursivoNacionalidad(lista, inicio, indicePivote - 1, ascendente);
            quicksortRecursivoNacionalidad(lista, indicePivote + 1, fin, ascendente);
        }
    }

    private int particionNacionalidad(ListaEnlazada<Cantante> lista, int inicio, int fin, boolean ascendente) throws VacioException, PosicionException {
        Cantante pivote = lista.get(fin);
        int i = inicio - 1;

        for (int j = inicio; j < fin; j++) {
            Cantante elementoActual = lista.get(j);
            if ((ascendente && elementoActual.getNacionalidad().compareToIgnoreCase(pivote.getNombre()) <= 0)
                    || (!ascendente && elementoActual.getNacionalidad().compareToIgnoreCase(pivote.getNombre()) >= 0)) {
                i++;
                lista.intercambioElementos(i, j);
            }
        }

        lista.intercambioElementos(i + 1, fin);
        return i + 1;
    }

    public ListaEnlazada<Cantante> quicksortGenero(ListaEnlazada<Cantante> lista, boolean ascendente) throws VacioException, PosicionException {
        quicksortRecursivoGenero(lista, 0, lista.getSize() - 1, ascendente);
        return lista;
    }

    private void quicksortRecursivoGenero(ListaEnlazada<Cantante> lista, int inicio, int fin, boolean ascendente) throws VacioException, PosicionException {
        if (inicio < fin) {
            int indicePivote = particionGenero(lista, inicio, fin, ascendente);
            quicksortRecursivoGenero(lista, inicio, indicePivote - 1, ascendente);
            quicksortRecursivoGenero(lista, indicePivote + 1, fin, ascendente);
        }
    }

    private int particionGenero(ListaEnlazada<Cantante> lista, int inicio, int fin, boolean ascendente) throws VacioException, PosicionException {
        Cantante pivote = lista.get(fin);
        GeneroDao gd = new GeneroDao();
        int i = inicio - 1;

        for (int j = inicio; j < fin; j++) {
            Cantante elementoActual = lista.get(j);
            boolean comparacion = gd.obtener(elementoActual.getId_genero()).equals(gd.obtener(pivote.getId_genero()));

            if ((ascendente && comparacion) || (!ascendente && !comparacion)) {
                i++;
                lista.intercambioElementos(i, j);
            }
        }

        lista.intercambioElementos(i + 1, fin);
        return i + 1;
    }

    public ListaEnlazada<Cantante> quicksortEdad(ListaEnlazada<Cantante> lista, boolean ascendente) throws VacioException, PosicionException {
        quicksortRecursivoEdad(lista, 0, lista.getSize() - 1, ascendente);
        return lista;
    }

    private void quicksortRecursivoEdad(ListaEnlazada<Cantante> lista, int inicio, int fin, boolean ascendente) throws VacioException, PosicionException {
        if (inicio < fin) {
            int indicePivote = particionEdad(lista, inicio, fin, ascendente);
            quicksortRecursivoEdad(lista, inicio, indicePivote - 1, ascendente);
            quicksortRecursivoEdad(lista, indicePivote + 1, fin, ascendente);
        }
    }

    private int particionEdad(ListaEnlazada<Cantante> lista, int inicio, int fin, boolean ascendente) throws VacioException, PosicionException {
        Cantante pivote = lista.get(fin);
        int i = inicio - 1;

        for (int j = inicio; j < fin; j++) {
            Cantante elementoActual = lista.get(j);
            if ((ascendente && elementoActual.getEdad() <= pivote.getEdad())
                    || (!ascendente && elementoActual.getEdad() >= pivote.getEdad())) {
                i++;
                lista.intercambioElementos(i, j);
            }
        }

        lista.intercambioElementos(i + 1, fin);
        return i + 1;
    }
}
