/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vista.utilidades;

import controlador.DAO.GeneroDao;
import controlador.ed.lista.ListaEnlazada;
import javax.swing.JComboBox;
import modelo.Genero;

/**
 *
 * @author mt3k
 */
public class UtilidadesVista {

    public static void cargarMarca(JComboBox cbx, GeneroDao gd) throws Exception {
        cbx.removeAllItems();

        ListaEnlazada<Genero> lista = gd.ordenarNombre(gd.listar());

        for (int i = 0; i < lista.size(); i++) {
            cbx.addItem(lista.get(i).getNombre().toUpperCase());
        }
    }
}
