/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista.modelo;

import controlador.DAO.GeneroDao;
import controlador.ed.lista.ListaEnlazada;
import javax.swing.table.AbstractTableModel;
import modelo.Cantante;

;

/**
 *
 * @author alyce
 */
public class ModeloTablaCantante extends AbstractTableModel {
    
    private ListaEnlazada<Cantante> lista = new ListaEnlazada<>();
    GeneroDao gd= new GeneroDao();
    
    public ListaEnlazada<Cantante> getLista() {
        return lista;
    }
    
    public void setLista(ListaEnlazada<Cantante> lista) {
        this.lista = lista;
    }
    
    @Override
    public int getColumnCount() {
        return 4;
    }
    
    @Override
    public int getRowCount() {
        return lista.size();
    }
    
    @Override
    public Object getValueAt(int i, int i1) {
        Cantante s = null;
        
        try {
            s = lista.get(i);
        } catch (Exception e) {
        }
        switch (i1) {
            
            case 0:
                return (s != null) ? s.getNombre() : "NO DEFINIDO";
            case 1:
                return (s != null) ? s.getNacionalidad() : "NO DEFINIDO";
            case 2:
                return (s != null) ? s.getEdad() : "NO DEFINIDO";
            case 3:
                return (s != null) ? gd.obtener(s.getId_genero()).getNombre() : "NO DEFINIDO";
            default:
                return null;
        }
    }
    
    @Override
    public String getColumnName(int column) {
        
        switch (column) {
            case 0:
                return "NOMBRE";
            case 1:
                return "NACIONALIDAD";
            case 2:
                return "EDAD";
            case 3:
                return "GENERO";
            default:
                return null;
        }
    }
    
}
